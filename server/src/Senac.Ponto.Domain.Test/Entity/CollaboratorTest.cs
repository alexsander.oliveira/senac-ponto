﻿using System;
using System.Linq;
using FluentAssertions;
using Senac.Ponto.Domain.Entities;
using Xunit;

namespace Senac.Ponto.Domain.Test.Entity
{
    public class CollaboratorTest
    {
        public CollaboratorTest() { }

        [Fact]
        public void CollaboratorEmpty()
        {
            //Arr
            var collaborator = new Collaborator("", Guid.Empty);
            //Act
            var valid = collaborator.IsValid();
            var resultValidation = collaborator.ValidationResult;

            //Assert
            valid.Should().BeFalse("Ocorreu uma falha Collaborator: retornou entidade válida");
            resultValidation.Errors.Should().NotBeEmpty("Ocorreu uma falha Collaborator: não retornou nenhum erro");
            resultValidation.Errors.Any(x => x.ErrorMessage == "O nome é obrigatório")
                .Should().BeTrue("Ocorreu uma falha Collaborator: não retornou o erro esperado");
            resultValidation.Errors.Any(x => x.ErrorMessage == "O id é obrigatório")
                .Should().BeTrue("Ocorreu uma falha Collaborator: não retornou o erro esperado");
        }

        [Fact]
        public void CollaboratorValid()
        {
            //Arr
            var collaborator = new Collaborator("Teste", Guid.NewGuid());
            //Act
            var valid = collaborator.IsValid();
            var resultValidation = collaborator.ValidationResult;

            //Assert
            valid.Should().BeTrue("Ocorreu uma falha Collaborator: retornou entidade inválida");
            resultValidation.Errors.Should().BeEmpty("Ocorreu uma falha Collaborator: retornou um erro");
        }
    }
}
