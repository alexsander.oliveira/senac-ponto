﻿
using System;
using System.Linq;
using FluentAssertions;
using Senac.Ponto.Domain.Entities;
using Xunit;

namespace Senac.Ponto.Domain.Test.Entity
{
    public class PointRecordTest
    {
        public PointRecordTest() { }

        [Fact]
        public void PointRecordEmpty()
        {
            //Arr
            var pointRecord = new PointRecord(Guid.Empty, null, DateTime.Now, 'A');
            //Act
            var valid = pointRecord.IsValid();
            var resultValidation = pointRecord.ValidationResult;

            //Assert
            valid.Should().BeFalse("Ocorreu uma falha PointRecord: retornou entidade válida");
            resultValidation.Errors.Should().NotBeEmpty("Ocorreu uma falha PointRecord: não retornou nenhum erro");

            resultValidation.Errors.Any(x => x.ErrorMessage == "O nome do colaborador é obrigatório")
                .Should().BeTrue("Ocorreu uma falha PointRecord: não retornou o erro esperado");
            resultValidation.Errors.Any(x => x.ErrorMessage == "O indicador de Entrada ou Saída inválido")
                .Should().BeTrue("Ocorreu uma falha PointRecord: não retornou o erro esperado");
        }

        [Fact]
        public void PointRecordValid()
        {
            //Arr
            var pointRecord = new PointRecord(Guid.NewGuid(), "Teste", DateTime.Now, 'E');
            //Act
            var valid = pointRecord.IsValid();
            var resultValidation = pointRecord.ValidationResult;

            //Assert
            valid.Should().BeTrue("Ocorreu uma falha PointRecord: retornou entidade inválida");
            resultValidation.Errors.Should().BeEmpty("Ocorreu uma falha PointRecord: retornou um erro");
        }

    }
}
