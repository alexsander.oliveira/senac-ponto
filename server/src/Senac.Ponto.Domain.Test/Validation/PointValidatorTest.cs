﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using Moq;
using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Domain.Interfaces.Repository;
using Senac.Ponto.Domain.Validations;
using Xunit;

namespace Senac.Ponto.Domain.Test.Validation
{
    public class PointValidatorTest
    {

        private Mock<ICollaboratorRepository> _collaboratorRepository;
        private PointRecord pointRecordValid;

        public PointValidatorTest()
        {
            _collaboratorRepository = new Mock<ICollaboratorRepository>();
            pointRecordValid = new PointRecord(Guid.NewGuid(), "Teste", DateTime.Now, 'E');
        }


        [Fact]
        public void ValidationInvalid()
        {
            // Arr
            var pointValidator = new PointValidator(
                _collaboratorRepository.Object,
                pointRecordValid);
            //Act
            var valid = pointValidator.IsValid();
            var resultValidation = pointValidator.GetValidationResult();

            //Assert
            valid.Should().BeFalse("Ocorreu uma falha PointValidator: não é inválida");
            resultValidation.Errors.Any(x => x.ErrorMessage == "O colaborador não foi encontrado")
                .Should().BeTrue("Ocorreu uma falha PointValidator: o erro não foi adicionado nas notificações");
        }

        [Fact]
        public void ValidationValid()
        {
            // Arr
            var collaboratorList = new List<Collaborator>()
            {
                new Collaborator("Teste", Guid.NewGuid())
            };

            _collaboratorRepository.Setup(x => x.Find(It.IsAny<Expression<Func<Collaborator, bool>>>()))
                .Returns(collaboratorList);

            var pointValidator = new PointValidator(
                _collaboratorRepository.Object,
                pointRecordValid);

            //Act
            var valid = pointValidator.IsValid();

            //Assert
            valid.Should().BeTrue("Ocorreu uma falha PointValidator: não é valida");
        }
    }
}
