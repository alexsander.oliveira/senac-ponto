using Senac.Ponto.Domain.Interfaces.Repository;

using Moq;
using Xunit;
using Senac.Ponto.Domain.Interfaces.UoW;
using Senac.Ponto.Domain.Services;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Domain.Entities;
using System;
using FluentAssertions;
using System.Linq;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Domain.Test
{
    public class CollaboratorServiceTest
    {
        public Mock<ICollaboratorRepository> _collaboratorRepositoryMock;
        public IDomainNotificationService _domainNotification;
        public Mock<IUnitOfWork> _unitOfWork;
        public CollaboratorDto collaboratorDtoValid;

        public CollaboratorServiceTest()
        {
            _collaboratorRepositoryMock = new Mock<ICollaboratorRepository>();
            _unitOfWork = new Mock<IUnitOfWork>();
            collaboratorDtoValid = new CollaboratorDto(Guid.NewGuid(), "Julio");
            _domainNotification = new DomainNotificationService();
        }

        [Fact]
        public void RegisterCollaboratorArgumentNull()
        {
            // Arr
            var collaboratorService = new CollaboratorService(
                _domainNotification,
                _collaboratorRepositoryMock.Object,
                _unitOfWork.Object);
            //Act
            collaboratorService.RegisterCollaborator(null);

            //Assert
            var hasNotification = _domainNotification.HasNotifications();
            var notifications = _domainNotification.GetNotifications();

            hasNotification.Should().BeTrue("Ocorreu uma falha CollaboratorService: n�o foi adicionado notifica��es");
            notifications.First().Value.Should()
                .Be("Argumento n�o � v�lido", "Ocorreu uma falha CollaboratorService: o erro n�o foi adicionado nas notifica��es");
        }

        [Fact]
        public void RegisterCollaboratorEntityInvalid()
        {
            //Arr
            var collaboratorService = new CollaboratorService(
                _domainNotification,
                _collaboratorRepositoryMock.Object,
                _unitOfWork.Object);

            //Act
            collaboratorService.RegisterCollaborator(new CollaboratorDto(Guid.NewGuid(), ""));

            //Assert
            var hasNotification = _domainNotification.HasNotifications();
            var notifications = _domainNotification.GetNotifications();

            hasNotification.Should().BeTrue("Ocorreu uma falha CollaboratorService: n�o foi adicionado notifica��es");
            notifications.First().Value.Should()
                .Be("O nome � obrigat�rio", "Ocorreu uma falha CollaboratorService: o erro n�o foi adicionado nas notifica��es");
        }

        [Fact]
        public void RegisterCollaboratorCommitError()
        {
            //Arr
            _unitOfWork.Setup(x => x.Commit()).Returns(false);

            var collaboratorService = new CollaboratorService(
                _domainNotification,
                _collaboratorRepositoryMock.Object,
                _unitOfWork.Object);

            //Act
            collaboratorService.RegisterCollaborator(collaboratorDtoValid);

            //Assert
            var hasNotification = _domainNotification.HasNotifications();
            var notifications = _domainNotification.GetNotifications();

            hasNotification.Should().BeTrue("Ocorreu uma falha CollaboratorService: n�o foi adicionado notifica��es");
            notifications.First().Value.Should()
                .Be("Ocorreu um erro ao salvar", "Ocorreu uma falha CollaboratorService: o erro n�o foi adicionado nas notifica��es");
        }

        [Fact]
        public void RegisterCollaboratorSuccess()
        {
            //Arr
            _unitOfWork.Setup(x => x.Commit()).Returns(true);

            var collaboratorService = new CollaboratorService(
                _domainNotification,
                _collaboratorRepositoryMock.Object,
                _unitOfWork.Object);

            //Act
            collaboratorService.RegisterCollaborator(collaboratorDtoValid);

            //Assert
            var hasNotification = _domainNotification.HasNotifications();
            var notifications = _domainNotification.GetNotifications();

            hasNotification.Should().BeFalse("Ocorreu uma falha CollaboratorService: existe notifica��es");
            notifications.Should()
                .BeEmpty("Ocorreu uma falha CollaboratorService: foi adicionado um erro nas notifica��es");
        }
    }
}
