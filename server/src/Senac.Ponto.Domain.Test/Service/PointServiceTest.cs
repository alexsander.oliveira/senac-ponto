﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using Moq;
using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Domain.Interfaces.Repository;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Domain.Interfaces.UoW;
using Senac.Ponto.Domain.Services;
using Senac.Ponto.Infra.Dto.Dto;
using Xunit;

namespace Senac.Ponto.Domain.Test.Service
{
    public class PointServiceTest
    {
        public Mock<IPointRepository> _pointRepositoryMock;
        public Mock<ICollaboratorRepository> _collaboratorRepositoryMock;

        public IDomainNotificationService _domainNotification;
        public Mock<IUnitOfWork> _unitOfWork;
        public PointDto pointDtoValid;
        public PointDto pointDtoInvalid;
        public PointServiceTest()
        {
            _pointRepositoryMock = new Mock<IPointRepository>();
            _collaboratorRepositoryMock = new Mock<ICollaboratorRepository>();
            _domainNotification = new DomainNotificationService();
            _unitOfWork = new Mock<IUnitOfWork>();
            pointDtoValid = new PointDto()
            {
                IdCollaborator = Guid.NewGuid(),
                Date = DateTime.Now,
                Name = "Teste",
                RecordType = 'E'
            };

            pointDtoInvalid = new PointDto()
            {
                IdCollaborator = Guid.Empty,
                Date = DateTime.Now,
                Name = "",
                RecordType = 'E'
            };
        }

        [Fact]
        public void RegisterDtoNull()
        {
            // Arr
            var pointService = new PointService(
                _domainNotification,
                _pointRepositoryMock.Object,
                _collaboratorRepositoryMock.Object,
                _unitOfWork.Object);
            //Act
            pointService.Register(null);

            //Assert
            var hasNotification = _domainNotification.HasNotifications();
            var notifications = _domainNotification.GetNotifications();

            hasNotification.Should().BeTrue("Ocorreu uma falha PointService: não foi adicionado notificações");
            notifications.First().Value.Should()
                .Be("Argumento não é válido", "Ocorreu uma falha PointService: o erro não foi adicionado nas notificações");
        }

        [Fact]
        public void RegisterEntityInvalid()
        {
            // Arr
            var pointService = new PointService(
                _domainNotification,
                _pointRepositoryMock.Object,
                _collaboratorRepositoryMock.Object,
                _unitOfWork.Object);
            //Act
            pointService.Register(pointDtoInvalid);

            //Assert
            var hasNotification = _domainNotification.HasNotifications();
            var notifications = _domainNotification.GetNotifications();

            hasNotification.Should().BeTrue("Ocorreu uma falha PointService: não foi adicionado notificações");
            notifications.Any(x => x.Value == "O nome do colaborador é obrigatório")
                .Should().BeTrue("Ocorreu uma falha PointService: o erro não foi adicionado nas notificações");
        }

        [Fact]
        public void RegisterValidationInvalid()
        {
            // Arr
            var pointService = new PointService(
                _domainNotification,
                _pointRepositoryMock.Object,
                _collaboratorRepositoryMock.Object,
                _unitOfWork.Object);
            //Act
            pointService.Register(pointDtoValid);

            //Assert
            var hasNotification = _domainNotification.HasNotifications();
            var notifications = _domainNotification.GetNotifications();

            hasNotification.Should().BeTrue("Ocorreu uma falha PointService: não foi adicionado notificações");
            notifications.Any(x => x.Value == "O colaborador não foi encontrado")
                .Should().BeTrue("Ocorreu uma falha PointService: o erro não foi adicionado nas notificações");
        }

        [Fact]
        public void RegisterCommitInvalid()
        {
            // Arr
            var collaboratorList = new List<Collaborator>()
            {
                new Collaborator("Teste", Guid.NewGuid())
            };

            _collaboratorRepositoryMock.Setup(x => x.Find(It.IsAny<Expression<Func<Collaborator, bool>>>()))
                .Returns(collaboratorList);
            _unitOfWork.Setup(x => x.Commit())
                .Returns(false);

            var pointService = new PointService(
                _domainNotification,
                _pointRepositoryMock.Object,
                _collaboratorRepositoryMock.Object,
                _unitOfWork.Object);
            //Act
            pointService.Register(pointDtoValid);

            //Assert
            var hasNotification = _domainNotification.HasNotifications();
            var notifications = _domainNotification.GetNotifications();

            hasNotification.Should()
                .BeTrue("Ocorreu uma falha PointService: não foi adicionado notificações");
            notifications.First().Value
                .Should().Be("Ocorreu um erro ao salvar",
                "Ocorreu uma falha PointService: o erro não foi adicionado nas notificações");
        }

        [Fact]
        public void RegisterSuccess()
        {
            // Arr
            var collaboratorList = new List<Collaborator>()
            {
                new Collaborator("Teste", Guid.NewGuid())
            };

            _collaboratorRepositoryMock.Setup(x => x.Find(It.IsAny<Expression<Func<Collaborator, bool>>>()))
                .Returns(collaboratorList);
            _unitOfWork.Setup(x => x.Commit())
                .Returns(true);

            var pointService = new PointService(
                _domainNotification,
                _pointRepositoryMock.Object,
                _collaboratorRepositoryMock.Object,
                _unitOfWork.Object);
            //Act
            pointService.Register(pointDtoValid);

            //Assert
            var hasNotification = _domainNotification.HasNotifications();

            hasNotification.Should()
                .BeFalse("Ocorreu uma falha PointService: foi adicionado notificações");
        }
    }
}
