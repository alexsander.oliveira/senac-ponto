﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Senac.Ponto.Infra.Identity;

namespace Senac.Ponto.Api.Configuration
{
	public class TokenService
	{
        public static string Secret = "194db60b-9097-4cfc-b989-a3dd159dbd2c";
        public static string GenerateToken(ApplicationUser user, IEnumerable<Claim> claims)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Secret);
            var identityClaims = new ClaimsIdentity();
            identityClaims.AddClaims(claims);
            identityClaims.AddClaim(new Claim(ClaimTypes.Name, user.UserName.ToString()));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identityClaims,
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
