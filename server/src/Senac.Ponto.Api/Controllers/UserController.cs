﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Senac.Ponto.Api.Configuration;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Infra.Dto.Dto;
using Senac.Ponto.Infra.Identity;
using Senac.Ponto.Infra.Identity.Models;

namespace Senac.Ponto.Api.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class UserController : BaseController
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly ICollaboratorService _collaboratorService;
		public UserController(
			UserManager<ApplicationUser> userManager,
			SignInManager<ApplicationUser> signInManager,
			ICollaboratorService collaboratorService,
			IDomainNotificationService domainNotificationService
			) : base(domainNotificationService)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_collaboratorService = collaboratorService;
        }

		[HttpPost]
		[Route("login")]
		public IActionResult Authenticate([FromBody] LoginViewModel model)
		{
			if (!ModelState.IsValid)
			{
				AddErrorModelInvalid();
				return Response(model);
			}

			var result = _signInManager.PasswordSignInAsync(model.Email, model.Password, false, true).Result;

			if ( result.Succeeded )
            {
				var user = _userManager.FindByEmailAsync(model.Email).Result;
				var userClaims = _userManager.GetClaimsAsync(user).Result;

				var token = TokenService.GenerateToken(user, userClaims);

				return Response( new
				{
					access_token = token,
					user = new
					{
						userName = user.UserName,
						id = user.Id
					}
				});
			}

			AddError(result.ToString(), "Ocorreu um erro ao efetuar o login");

			return Response(model);
        }

		[HttpPost]
		[Route("register")]
		public IActionResult Register([FromBody] RegisterViewModel model)
		{
			if (!ModelState.IsValid)
			{
				AddErrorModelInvalid();
				return Response(model);
			}

			var user = new ApplicationUser { UserName = model.Email, Email = model.Email };

			var result = _userManager.CreateAsync(user, model.Password).Result;

			if (result.Succeeded)
			{
				_userManager.AddClaimAsync(user, new Claim("Dot", "Record"));

				var collaborator = new CollaboratorDto(Guid.Parse(user.Id), model.Name);
				_collaboratorService.RegisterCollaborator(collaborator);

				if ( ! ValidOperation() )
                {
					_userManager.DeleteAsync(user);
					return Response(model);
                }
			}

			AddErrorsIdentity(result);
			return Response(model);
		}
	}
}
