﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Senac.Ponto.Domain.Interfaces.Query;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReportController : BaseController
    {
        private readonly IReportPointQuery _reportPointQuery;
        public ReportController(
            IReportPointQuery reportPointQuery,
            IDomainNotificationService domainNotificationService
            ) : base( domainNotificationService )
        {
            _reportPointQuery = reportPointQuery;
        }

        [HttpGet]
        [Route("point")]
        [Authorize(Policy = "AdmPoint")]
        public IActionResult Report([FromQuery] ReportPointResearchDto filter)
        {
            var result = _reportPointQuery.Report(filter);
            return Response(result);
        }
    }
}
