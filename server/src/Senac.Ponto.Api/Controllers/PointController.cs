﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Senac.Ponto.Domain.Interfaces.Query;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PointController : BaseController
    {
        private IPointService _pointService;
        private IPointQuery _pointQuery;
        public PointController(
            IPointService pointService,
            IPointQuery pointQuery,
            IDomainNotificationService domainNotificationService
            ) : base( domainNotificationService )
        {
            _pointService = pointService;
            _pointQuery = pointQuery;
        }

        [HttpPost]
        [Route("record")]
        [Authorize(Policy = "RecordPoint")]
        public IActionResult PointRecord([FromBody] PointDto dto)
        {
            if (!ModelState.IsValid)
            {
                AddErrorModelInvalid();
                return Response(dto);
            }

            _pointService.Register(dto);

            return Response(dto);
        }

        [HttpGet]
        [Route("list-page")]
        [Authorize( Policy = "RecordPoint")]
        public IActionResult List([FromQuery] PointResearchDto filter)
        {
            var result = _pointQuery.ListPagination(filter);
            return Response(result);
        }

    }
}
