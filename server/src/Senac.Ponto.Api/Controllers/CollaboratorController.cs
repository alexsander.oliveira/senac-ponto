﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Senac.Ponto.Domain.Interfaces.Query;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CollaboratorController : BaseController
    {
        private readonly ICollaboratorQuery _collaboratorQuery;
        public CollaboratorController(
            ICollaboratorQuery collaboratorQuery,
            IDomainNotificationService domainNotificationService
            ) : base( domainNotificationService )
        {
            _collaboratorQuery = collaboratorQuery;
        }

        [HttpGet]
        [Route("list")]
        [Authorize(Policy = "AdmPoint")]
        public IEnumerable<CollaboratorListDto> List()
        {
            return _collaboratorQuery.List();            
        }
    }
}
