﻿using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Senac.Ponto.Domain.Interfaces.Services;

namespace Senac.Ponto.Api.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        private IDomainNotificationService _notifications;
        public BaseController(IDomainNotificationService notifications)
        {
            _notifications = notifications;
        }

        protected new IActionResult Response(object result = null)
        {
            if (! _notifications.HasNotifications() )
            {
                return Ok(new
                {
                    success = true,
                    data = result
                });
            }

            return BadRequest(new
            {
                success = false,
                errors = _notifications.GetNotifications().Select(n => n.Value)
            });
        }

        protected bool ValidOperation()
        {
            return !_notifications.HasNotifications();
        }

        protected void AddError(string contexto, string message)
        {
            _notifications.AddNotification(contexto, message);
        }

        protected void AddErrorsIdentity(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                _notifications.AddNotification(result.ToString(), error.Description);
            }
        }

        protected void AddErrorModelInvalid()
        {
            var erros = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var erro in erros)
            {
                var erroMsg = erro.Exception == null ? erro.ErrorMessage : erro.Exception.Message;
                _notifications.AddNotification(string.Empty, erroMsg);
            }
        }
    }
}
