﻿using Microsoft.Extensions.DependencyInjection;
using Senac.Ponto.Infra.Data.Repository;
using Senac.Ponto.Domain.Services;
using Senac.Ponto.Domain.Interfaces.Repository;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Infra.Data.Context;
using Senac.Ponto.Infra.Data.UoW;
using Senac.Ponto.Domain.Interfaces.UoW;
using Senac.Ponto.Domain.Interfaces.Query;
using Senac.Ponto.Infra.Data.Query;

namespace Senac.Ponto.Infra.IoC
{
    public class NativeInjector
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //Data
            services.AddDbContext<SenacDbContext>();

            services.AddScoped<SenacDbContext>();

            // Domain
            services.AddScoped<IDomainNotificationService, DomainNotificationService>();
            services.AddScoped<ICollaboratorService, CollaboratorService>();
            services.AddScoped<IPointService, PointService>();

            // Infra - Data
            services.AddScoped<ICollaboratorRepository, CollaboratorRepository>();
            services.AddScoped<IPointRepository, PointRepository>();

            services.AddScoped<IPointQuery, PointQuery>();
            services.AddScoped<IReportPointQuery, ReportPointQuery>();
            services.AddScoped<ICollaboratorQuery, CollaboratorQuery>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
