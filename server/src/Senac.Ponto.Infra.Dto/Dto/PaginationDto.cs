﻿using System.Collections.Generic;

namespace Senac.Ponto.Infra.Dto.Dto
{
    public class PaginationDto <TEntity>
    {
        public IEnumerable<TEntity> Records { get; set; }
        public string Sort { get; set; }
        public string SortDirection { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
}
}
