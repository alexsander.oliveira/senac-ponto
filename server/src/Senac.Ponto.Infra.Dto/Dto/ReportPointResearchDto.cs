﻿using System;

namespace Senac.Ponto.Infra.Dto.Dto
{
    public class ReportPointResearchDto
    {
        public Guid IdCollaborator { get; set; }
        public string Sort { get; set; }
        public string SortDirection { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
