﻿using System;

namespace Senac.Ponto.Infra.Dto.Dto
{
    public class ReportPointListDto
    {
        public Guid IdCollaborator { get; set; }
        public string Name { get; set; }
        public DateTimeOffset Date { get; set; }
        public decimal HoursWorked { get; set; }
    }
}
