﻿using System;

namespace Senac.Ponto.Infra.Dto.Dto
{
    public class PointListDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public char RecordType { get; set; }
    }
}
