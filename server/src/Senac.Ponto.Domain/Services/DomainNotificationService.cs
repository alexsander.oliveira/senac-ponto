﻿using System;
using System.Collections.Generic;
using System.Linq;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Domain.Notification;

namespace Senac.Ponto.Domain.Services
{
    public class DomainNotificationService : IDomainNotificationService
    {
        private List<DomainNotification> _notifications;

        public DomainNotificationService()
        {
            _notifications = new List<DomainNotification>();
        }

        public void AddNotification(DomainNotification notification)
        {
            _notifications.Add(notification);
        }

        public void AddNotification(string key, string value)
        {
            _notifications.Add(new DomainNotification(key, value));
        }

        public bool HasNotifications()
        {
            return _notifications.Any();
        }
        public List<DomainNotification> GetNotifications()
        {
            return _notifications;
        }
    }
}
