﻿using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Domain.Interfaces.Repository;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Domain.Interfaces.UoW;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Domain.Services
{
    public class CollaboratorService : 
        BaseService<Collaborator>,
        ICollaboratorService
    {
        public ICollaboratorRepository _collaboratorRepository;
        public IUnitOfWork _unitOfWork;
        public CollaboratorService(
            IDomainNotificationService domainNotificationService,
            ICollaboratorRepository collaboratorRepository,
            IUnitOfWork unitOfWork
            ) : base(domainNotificationService)
        {
            _domainNotificationService = domainNotificationService;
            _collaboratorRepository = collaboratorRepository;
            _unitOfWork = unitOfWork;
        }

        public void RegisterCollaborator(CollaboratorDto collaborator)
        {
            if ( collaborator == null )
            {
                AddNotification("Collaborator", "Argumento não é válido");

                return;
            }

            var entity = new Collaborator(collaborator.Name, collaborator.Id);

            if ( !entity.IsValid() )
            {
                AddEntityNotification(entity);

                return;
            }

            _collaboratorRepository.AddCollaborator(entity);

            if ( !_unitOfWork.Commit() )
            {
                AddNotification("UnitOfWork", "Ocorreu um erro ao salvar");
            }
        }
    }
}
