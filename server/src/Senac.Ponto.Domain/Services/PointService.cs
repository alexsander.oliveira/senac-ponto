﻿using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Domain.Interfaces.Repository;
using Senac.Ponto.Domain.Interfaces.Services;
using Senac.Ponto.Domain.Interfaces.UoW;
using Senac.Ponto.Domain.Validations;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Domain.Services
{
    public class PointService :
        BaseService<PointRecord>,
        IPointService
    {
        private IPointRepository _pointRepository;
        private ICollaboratorRepository _collaboratorRepository;
        private IUnitOfWork _unitOfWork;
        public PointService(
            IDomainNotificationService domainNotificationService,
            IPointRepository pointRepository,
            ICollaboratorRepository collaboratorRepository,
            IUnitOfWork unitOfWork
            )
            :base(domainNotificationService)
        {
            _pointRepository = pointRepository;
            _collaboratorRepository = collaboratorRepository;
            _unitOfWork = unitOfWork;
        }

        public void Register(PointDto dto)
        {
            if (dto == null)
            {
                AddNotification("Point", "Argumento não é válido");

                return;
            }

            var entity = new PointRecord(
                dto.IdCollaborator,
                dto.Name,
                dto.Date,
                dto.RecordType );

            if (!entity.IsValid())
            {
                AddEntityNotification(entity);

                return;
            }

            var validator = new PointValidator(_collaboratorRepository, entity);
            if (!validator.IsValid())
            {
                AddNotificationValidations(validator.GetValidationResult());

                return;
            }

            _pointRepository.AddPoint(entity);


            if (!_unitOfWork.Commit())
            {
                AddNotification("UnitOfWork", "Ocorreu um erro ao salvar");
            }
        }
    }
}
