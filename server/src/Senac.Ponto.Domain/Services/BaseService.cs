﻿using FluentValidation.Results;
using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Domain.Interfaces.Services;

namespace Senac.Ponto.Domain.Services
{
    public abstract class BaseService<T>
    {
        public IDomainNotificationService _domainNotificationService; 
        public BaseService(IDomainNotificationService domainNotificationService)
        {
            _domainNotificationService = domainNotificationService;
        }

        protected void AddNotification(string context, string message)
        {
            _domainNotificationService.AddNotification(context, message);
        }

        protected void AddEntityNotification(Entity<T> entity)
        {
            foreach (var error in entity.ValidationResult.Errors)
            {
                _domainNotificationService.AddNotification(entity.GetType().Name, error.ErrorMessage);
            }
        }

        protected void AddNotificationValidations(ValidationResult validationResult)
        {
            foreach (var error in validationResult.Errors)
            {
                _domainNotificationService.AddNotification(error.PropertyName, error.ErrorMessage);
            }
        }
    }
}
