﻿using FluentValidation;
using FluentValidation.Results;

namespace Senac.Ponto.Domain.Validations
{
    public abstract class BaseValidator<TEntity> : AbstractValidator<TEntity>
    {
        protected TEntity _entity;
        protected ValidationResult ValidationResult;

        public BaseValidator(TEntity entity)
        {
            _entity = entity;
        }

        public bool IsValid()
        {
            ValidationResult = Validate(_entity);
            return ValidationResult.IsValid;
        }

        public ValidationResult GetValidationResult()
        {
            return ValidationResult;
        }
    }
}
