﻿using System.Linq;
using FluentValidation;
using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Domain.Interfaces.Repository;

namespace Senac.Ponto.Domain.Validations
{
    public class PointValidator : BaseValidator<PointRecord>
    {
        private ICollaboratorRepository _collaboratorRepository;
        public PointValidator(ICollaboratorRepository collaboratorRepository, PointRecord entity)
            : base(entity)
        {
            _collaboratorRepository = collaboratorRepository;
            AddRules();
        }

        private void AddRules()
        {
            RuleFor(x => x)
                .Must(VerifyCollaborator)
                .WithMessage("O colaborador não foi encontrado");

            RuleFor(x => x)
                .Must(VerifyCollaboratorName)
                .WithMessage("O nome do colaborador não corresponde ao registrado");
        }

        protected bool VerifyCollaborator(PointRecord entidade)
        {

            var registro = _collaboratorRepository
                .Find(x => x.Id == entidade.IdCollaborator )
                .FirstOrDefault();

            if ( registro == null )
            {
                return false;
            }

            return true;
        }

        protected bool VerifyCollaboratorName(PointRecord entidade)
        {

            var registro = _collaboratorRepository
                .Find(x => x.Id == entidade.IdCollaborator 
                    && x.Name == entidade.Name)
                .FirstOrDefault();

            if ( registro == null )
            {
                return false;
            }

            return true;
        }
    }
}
