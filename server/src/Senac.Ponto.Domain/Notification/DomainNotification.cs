﻿
namespace Senac.Ponto.Domain.Notification
{
    public class DomainNotification
    {
        public DomainNotification(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }

        public string Key { get; private set; }
        public string Value { get; private set; }
    }
}
