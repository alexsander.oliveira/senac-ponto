﻿
using System;
using FluentValidation;

namespace Senac.Ponto.Domain.Entities
{
    public class Collaborator : Entity<Collaborator>
    {
        public Collaborator(string name, Guid id)
        {
            Name = name;
            Id = id;

            AddValidations();
        }
        public string Name { get; private set; }

        public override bool IsValid()
        {            
            ValidationResult = Validate(this);
            return ValidationResult.IsValid;
        }

        private void AddValidations()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("O nome é obrigatório");

            RuleFor(c => c.Id)
                .NotEmpty().WithMessage("O id é obrigatório");
        }
    }
}
