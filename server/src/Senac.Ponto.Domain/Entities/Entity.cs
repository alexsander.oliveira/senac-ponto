﻿using System;
using FluentValidation;
using FluentValidation.Results;

namespace Senac.Ponto.Domain.Entities
{
    public abstract class Entity<T> : AbstractValidator<T>
    {
        public Guid Id { get; protected set; }

        public abstract bool IsValid();
        public ValidationResult ValidationResult { get; protected set; }
    }
}
