﻿
using System;
using FluentValidation;

namespace Senac.Ponto.Domain.Entities
{
    public class PointRecord : Entity<PointRecord>
    {
        public PointRecord(
            Guid idCollaborator, 
            string name,
            DateTime date,
            char recordType )
        {
            Id = Guid.NewGuid();
            IdCollaborator = idCollaborator;
            Name = name;
            Date = date;
            RecordType = recordType;

            AddValidations();
        }
        public Guid IdCollaborator { get; private set; }
        public string Name { get; private set; }
        public DateTime Date { get; private set; }
        public char RecordType { get; private set; }

        public override bool IsValid()
        {            
            ValidationResult = Validate(this);
            return ValidationResult.IsValid;
        }

        private void AddValidations()
        {
            RuleFor(c => c.Id)
                .NotEmpty().WithMessage("O id do colaborador é obrigatório");

            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("O nome do colaborador é obrigatório");

            RuleFor(c => c.Date)
                .NotEmpty().WithMessage("A data e horário é obrigatório");

            RuleFor(c => c.RecordType)
                .NotEmpty()
                .WithMessage("O indicador de Entrada ou Saída é obrigatório");

            RuleFor(c => c.RecordType)
                .Must(x => x == 'S' || x == 'E')
                .WithMessage("O indicador de Entrada ou Saída inválido");

        }
    }
}
