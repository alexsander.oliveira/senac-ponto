﻿namespace Senac.Ponto.Domain.Interfaces.UoW
{
    public interface IUnitOfWork
    {
        bool Commit();
    }
}
