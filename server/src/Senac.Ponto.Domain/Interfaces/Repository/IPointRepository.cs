﻿using Senac.Ponto.Domain.Entities;

namespace Senac.Ponto.Domain.Interfaces.Repository
{
    public interface IPointRepository : IRepositoryBase<PointRecord>
    {
        void AddPoint(PointRecord entity);
    }
}
