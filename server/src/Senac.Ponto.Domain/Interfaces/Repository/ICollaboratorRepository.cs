﻿using Senac.Ponto.Domain.Entities;

namespace Senac.Ponto.Domain.Interfaces.Repository
{
    public interface ICollaboratorRepository : IRepositoryBase<Collaborator>
    {
        public void AddCollaborator(Collaborator entity);
    }
}
