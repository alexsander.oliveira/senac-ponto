﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Senac.Ponto.Domain.Interfaces.Repository
{
    public interface IRepositoryBase<TEntity>
    {
        public void Add(TEntity entity);
        void Update(TEntity entity);
        TEntity GetById(Guid id);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> expression);
        void Delete(Guid id);
    }
}
