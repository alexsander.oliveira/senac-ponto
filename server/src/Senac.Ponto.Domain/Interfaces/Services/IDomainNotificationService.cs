﻿
using System.Collections.Generic;
using Senac.Ponto.Domain.Notification;

namespace Senac.Ponto.Domain.Interfaces.Services
{
    public interface IDomainNotificationService
    {
        void AddNotification(DomainNotification notification);
        void AddNotification(string key, string value);
        bool HasNotifications();
        List<DomainNotification> GetNotifications();
    }
}
