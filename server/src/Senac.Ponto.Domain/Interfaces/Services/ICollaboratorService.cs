﻿using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Domain.Interfaces.Services
{
    public interface ICollaboratorService
    {
        void RegisterCollaborator(CollaboratorDto collaborator);
    }
}
