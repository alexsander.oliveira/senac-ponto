﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Domain.Interfaces.Query
{
    public interface ICollaboratorQuery
    {
        IEnumerable<CollaboratorListDto> List();
    }
}
