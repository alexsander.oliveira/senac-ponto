﻿using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Domain.Interfaces.Query
{
    public interface IReportPointQuery
    {
        PaginationDto<ReportPointListDto> Report(ReportPointResearchDto filter);
    }
}
