﻿using System.Collections.Generic;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Domain.Interfaces.Query
{
    public interface IPointQuery
    {
        PaginationDto<PointListDto> ListPagination(PointResearchDto filter);
    }
}
