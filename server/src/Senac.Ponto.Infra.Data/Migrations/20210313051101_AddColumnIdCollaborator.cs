﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Senac.Ponto.Infra.Data.Migrations
{
    public partial class AddColumnIdCollaborator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "IdCollaborator",
                schema: "dbo",
                table: "PointRecord",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdCollaborator",
                schema: "dbo",
                table: "PointRecord");
        }
    }
}
