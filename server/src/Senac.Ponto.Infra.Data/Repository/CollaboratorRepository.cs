﻿using System;
using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Domain.Interfaces.Repository;
using Senac.Ponto.Infra.Data.Context;

namespace Senac.Ponto.Infra.Data.Repository
{
    public class CollaboratorRepository :
        RepositoryBase<Collaborator>,
        ICollaboratorRepository
    {
        public SenacDbContext _context;
        public CollaboratorRepository(SenacDbContext context)
            :base(context)
        {
            _context = context;
        }
        public void AddCollaborator(Collaborator entity)
        {
            Add(entity);
        }
    }
}
