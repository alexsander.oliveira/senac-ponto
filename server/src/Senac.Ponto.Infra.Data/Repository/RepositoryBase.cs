﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Infra.Data.Context;

namespace Senac.Ponto.Infra.Data.Repository
{
    public abstract class RepositoryBase<TEntity> 
        where TEntity : Entity<TEntity>
    {
        protected SenacDbContext _context;
        protected DbSet<TEntity> _dbSet;
        public RepositoryBase(SenacDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _context.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _context.Update(entity);
        }
        public TEntity GetById(Guid id)
        {
            return _dbSet.AsNoTracking().FirstOrDefault(t => t.Id == id);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> expression)
        {
            return _dbSet.AsNoTracking().Where(expression);
        }

        public void Delete(Guid id)
        {
            _context.Remove(_dbSet.Find(id));
        }
    }
}
