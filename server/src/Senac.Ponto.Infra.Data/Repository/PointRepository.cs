﻿using System.Linq;
using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Domain.Interfaces.Repository;
using Senac.Ponto.Infra.Data.Context;

namespace Senac.Ponto.Infra.Data.Repository
{
    public class PointRepository 
        : RepositoryBase<PointRecord>,
        IPointRepository
    {
        private SenacDbContext _context;

        public PointRepository(SenacDbContext context)
            : base(context)
        {
            _context = context;
        }

        public void AddPoint(PointRecord entity)
        {
            Add(entity);
        }
    }
}
