﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senac.Ponto.Domain.Entities;

namespace Senac.Ponto.Infra.Data.Mapping
{
    public class PointRecordMapping : IEntityTypeConfiguration<PointRecord>
    {
        public void Configure(EntityTypeBuilder<PointRecord> builder)
        {
            // table
            builder.ToTable("PointRecord", "dbo");

            // key
            builder.HasKey(t => t.Id);

            builder.Property(t => t.IdCollaborator)
                .IsRequired()
                .HasColumnName("IdCollaborator")
                .HasColumnType("uniqueidentifier");

            builder.Property(t => t.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasColumnType("varchar(200)")
                .HasMaxLength(200);

            builder.Property(t => t.RecordType)
                .HasColumnName("RecordType")
                .HasColumnType("char(1)")
                .HasMaxLength(1);

            builder.Property(t => t.Date)
                .HasColumnName("Date")
                .HasColumnType("datetime");

            builder.Ignore(t => t.ValidationResult);
            builder.Ignore(e => e.CascadeMode);
        }
    }
}
