﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senac.Ponto.Domain.Entities;

namespace Senac.Ponto.Infra.Data.Mapping
{
    public class CollaboratorMapping : IEntityTypeConfiguration<Collaborator>
    {
        public void Configure(EntityTypeBuilder<Collaborator> builder)
        {
            // table
            builder.ToTable("Collaborator", "dbo");

            // key
            builder.HasKey(t => t.Id);

            builder.Property(t => t.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasColumnType("varchar(200)")
                .HasMaxLength(200);

            builder.Ignore(t => t.ValidationResult);
            builder.Ignore(e => e.CascadeMode);
        }
    }
}
