﻿using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Senac.Ponto.Domain.Interfaces.Query;
using Senac.Ponto.Infra.Data.Context;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Infra.Data.Query
{
    public class PointQuery : IPointQuery
    {
        private SenacDbContext _context;

        public PointQuery(SenacDbContext context)
        {
            _context = context;
        }

        public PaginationDto<PointListDto> ListPagination(PointResearchDto filter)
        {
            using (SqlConnection conexao = new SqlConnection(
                _context.Database.GetConnectionString()))
            {
                var builder = new SqlBuilder();
                var template = builder.AddTemplate(@"select /**select**/ from dbo.PointRecord PR " +
                        "/**where**/ ORDER BY PR.Date desc " +
                        "OFFSET (@PageIndex * @PageSize) ROWS " + 
                        "FETCH NEXT @PageSize ROWS ONLY");

                var templateCount = builder.AddTemplate(@"SELECT COUNT(*) from dbo.PointRecord PR /**where**/ ");

                builder.Select("PR.Id AS " + nameof(PointListDto.Id));
                builder.Select("PR.Name AS " + nameof(PointListDto.Name));
                builder.Select("PR.Date AS " + nameof(PointListDto.Date));
                builder.Select("PR.RecordType AS " + nameof(PointListDto.RecordType));

                builder.AddParameters(new { PageIndex = filter.PageIndex }); 
                builder.AddParameters(new { PageSize = filter.PageSize });

                builder.Where("PR.IdCollaborator = @IdCollaborator", new { IdCollaborator = filter.IdCollaborator });

                var pagina = new PaginationDto<PointListDto>();
                pagina.PageIndex = filter.PageIndex;
                pagina.PageSize = filter.PageSize;

                pagina.TotalRecords = conexao.Query<int>(templateCount.RawSql, template.Parameters).Single();
                pagina.Records = conexao.Query<PointListDto>(template.RawSql, template.Parameters);

                return pagina;
            }
        }
    }
}
