﻿using System;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Senac.Ponto.Domain.Interfaces.Query;
using Senac.Ponto.Infra.Data.Context;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Infra.Data.Query
{
    public class ReportPointQuery : IReportPointQuery
    {
        private SenacDbContext _context;

        public ReportPointQuery(SenacDbContext context)
        {
            _context = context;
        }

        public PaginationDto<ReportPointListDto> Report(ReportPointResearchDto filter)
        {
            using (SqlConnection conexao = new SqlConnection(
                _context.Database.GetConnectionString()))
            {
                var builder = new SqlBuilder();
                var template = builder.AddTemplate(@"select /**select**/ from dbo.PointRecord PR 
                        /**where**/ /**groupby**/ /**orderby**/ 
                        OFFSET (@PageIndex * @PageSize) ROWS 
                        FETCH NEXT @PageSize ROWS ONLY");

                var templateCount = builder.AddTemplate(@"
                    SELECT 
                        COUNT(1)
                    FROM 
                        (
                            select /**select**/ from dbo.PointRecord PR
                            /**where**/
                            /**groupby**/ 
                        ) T ");

                builder.Select("PR.Name AS " + nameof(ReportPointListDto.Name));
                builder.Select("CONVERT(DATE, PR.Date) AS " + nameof(ReportPointListDto.Date));
                builder.Select("PR.IdCollaborator AS " + nameof(ReportPointListDto.IdCollaborator));

                builder.Select(@"
                ( SELECT
				    CAST(DATEPART(HOUR, CONVERT(TIME, DATEADD(ms,  TotalOut - TotalEntry, '00:00:00.000'))) AS NUMERIC(10,3)) +
				    (CAST(DATEPART(MINUTE, CONVERT(TIME, DATEADD(ms,  TotalOut - TotalEntry, '00:00:00.000'))) AS NUMERIC(10,3))  / 60) +
				    (CAST(DATEPART(SECOND, CONVERT(TIME, DATEADD(ms,  TotalOut - TotalEntry, '00:00:00.000'))) AS NUMERIC(10,3)) / 3600 )
			        FROM ( 
			        SELECT
				        SUM(DATEDIFF(ms, '00:00:00.000', (CASE WHEN PRI.RecordType = 'E' THEN CONVERT(TIME, PRI.Date) ELSE NULL END))) AS TotalEntry,
				        SUM(DATEDIFF(ms, '00:00:00.000', (CASE WHEN PRI.RecordType = 'S' THEN CONVERT(TIME, PRI.Date) ELSE NULL END))) AS TotalOut,
				        PRI.IdCollaborator,
				        CONVERT(DATE, PRI.Date) AS Date
			        FROM dbo.PointRecord PRI
			        WHERE 
				        PRI.IdCollaborator = @IdCollaborator
                        AND CONVERT(DATE, PRI.Date) BETWEEN  CONVERT(DATE, PR.Date) AND CONVERT(DATE, PR.Date)
			        GROUP BY PRI.IdCollaborator, CONVERT(DATE, PRI.Date) )  T1 ) AS " + nameof(ReportPointListDto.HoursWorked));

                builder.AddParameters(new { PageIndex = filter.PageIndex }); 
                builder.AddParameters(new { PageSize = filter.PageSize });

                builder.Where("PR.IdCollaborator = @IdCollaborator", new { filter.IdCollaborator });

                builder.OrderBy("CONVERT(DATE, PR.Date)");
                builder.GroupBy("CONVERT(DATE, PR.Date), PR.IdCollaborator, PR.Name");

                var pagina = new PaginationDto<ReportPointListDto>();
                pagina.PageIndex = filter.PageIndex;
                pagina.PageSize = filter.PageSize;

                pagina.TotalRecords = conexao.QueryFirstOrDefault<int>(templateCount.RawSql, template.Parameters);
                pagina.Records = conexao.Query<ReportPointListDto>(template.RawSql, template.Parameters);

                return pagina;
            }
        }
    }
}
