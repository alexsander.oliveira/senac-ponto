﻿using System;
using System.Collections.Generic;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Senac.Ponto.Domain.Interfaces.Query;
using Senac.Ponto.Infra.Data.Context;
using Senac.Ponto.Infra.Dto.Dto;

namespace Senac.Ponto.Infra.Data.Query
{
    public class CollaboratorQuery : ICollaboratorQuery
    {
        private readonly SenacDbContext _context;
        public CollaboratorQuery(SenacDbContext context)
        {
            _context = context;
        }

        public IEnumerable<CollaboratorListDto> List()
        {
            using (SqlConnection conexao = new SqlConnection(
                _context.Database.GetConnectionString()))
            {
                var builder = new SqlBuilder();
                var template = builder.AddTemplate(@"select top 5000
                            /**select**/ from dbo.Collaborator Co " +
                        "/**where**/ /**orderby**/");

                builder.Select("Co.Id AS " + nameof(CollaboratorListDto.Id));
                builder.Select("Co.Name AS " + nameof(CollaboratorListDto.Name));

                builder.OrderBy("Co.Name");

                return conexao.Query<CollaboratorListDto>(template.RawSql, template.Parameters);
            }
        }
    }
}
