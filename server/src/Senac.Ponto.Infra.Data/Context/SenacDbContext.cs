﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Senac.Ponto.Domain.Entities;
using Senac.Ponto.Infra.Data.Mapping;

namespace Senac.Ponto.Infra.Data.Context
{
    public class SenacDbContext : DbContext
    {
        public SenacDbContext()
        {
        }
        public DbSet<Collaborator> Collaborators { get; set; }
        public DbSet<PointRecord> Points { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CollaboratorMapping());
            modelBuilder.ApplyConfiguration(new PointRecordMapping());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));

            base.OnConfiguring(optionsBuilder);
        }
    }
}
