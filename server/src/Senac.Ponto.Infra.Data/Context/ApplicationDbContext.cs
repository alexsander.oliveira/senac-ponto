﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Senac.Ponto.Infra.Identity;

namespace Senac.Ponto.Infra.Data.Context
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser> 
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
		   : base(options) { }
			
	}
}
