﻿using System;
using Senac.Ponto.Domain.Interfaces.UoW;
using Senac.Ponto.Infra.Data.Context;

namespace Senac.Ponto.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly SenacDbContext _context;

        public UnitOfWork(SenacDbContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            var rowsAffected = _context.SaveChanges();
            return rowsAffected > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
