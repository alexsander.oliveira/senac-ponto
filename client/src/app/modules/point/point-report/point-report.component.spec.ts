import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointReportComponent } from './point-report.component';

describe('PointReportComponent', () => {
  let component: PointReportComponent;
  let fixture: ComponentFixture<PointReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
