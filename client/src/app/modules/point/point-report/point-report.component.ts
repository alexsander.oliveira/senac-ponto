import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { merge, Observable, Subscription } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { Collaborator } from '../models/collaborator';
import { Page } from '../models/page';

import { PointListReport } from '../models/point-list-report';
import { ReportService } from '../services/report.service';

@Component({
  selector: 'app-point-report',
  templateUrl: './point-report.component.html',
  styleUrls: ['./point-report.component.css'],
})
export class PointReportComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['name', 'date', 'hoursWorked'];
  dataSource: MatTableDataSource<PointListReport>;
  panelOpenState = true;
  totalRecords: number;
  pageIndex: number;
  pageSize: number;
  collaborators: Collaborator[];
  form: FormGroup;
  idCollaboratorFilter = '00000000-0000-0000-0000-000000000000';

  collaboratorsSubscription: Subscription;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private reportService: ReportService, private fb: FormBuilder) {

    this.form = this.fb.group({
      collaborator: ['', Validators.required],
    });

    this.listAllCollaborators();

    this.dataSource = new MatTableDataSource();
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
            return this.listReport(
              this.sort.start,
              this.sort.direction,
              this.paginator.pageIndex,
              this.paginator.pageSize
            );
        }),
        map((data: any) => {
          this.totalRecords = data.data.totalRecords;
          this.pageIndex = data.data.pageIndex;
          this.pageSize = data.data.pageSize;
          return data.data;
        })
      )
      .subscribe((data) => {
        this.dataSource.data = data.records;
        this.paginator.length = data.totalRecords;
      });
  }

  ngOnInit(): void {}

  listReport(sort: any, sortDirection: any, pageIndex: any, pageSize: any) {
    let filter = {
      idCollaborator: this.idCollaboratorFilter,
      sort: sort,
      sortDirection: sortDirection,
      pageIndex: pageIndex,
      pageSize: pageSize,
    };

    return this.reportService.listPage(filter);
  }

  listAllCollaborators() {
    this.collaboratorsSubscription = this.reportService
      .listAllCollaborators()
      .subscribe((res) => {
        this.collaborators = res;
      });
  }

  onSubmit() {
    if ( this.form.invalid ) {
      return;
    }

    var collaborator = this.form.get('collaborator').value;
    this.idCollaboratorFilter = collaborator.id;

    this.listReport('asc', '', 0, 5).subscribe(result => {
        this.totalRecords = result.data.totalRecords;
        this.pageIndex = result.data.pageIndex;
        this.pageSize = result.data.pageSize;
        this.dataSource.data = result.data.records;
        this.paginator.length = result.data.totalRecords;
    })
  }

  ngOnDestroy(): void {
    if (this.collaboratorsSubscription) {
      this.collaboratorsSubscription.unsubscribe();
    }
  }
}
