import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PointListComponent } from '../point/point-list/point-list.component';
import { PointRegisterComponent } from './point-register/point-register.component';
import { PointReportComponent } from './point-report/point-report.component';
import { AuthGuard } from '../../core/helpers/auth.guard';


const routes: Routes = [
  {
    path: 'point',
    canActivate: [ AuthGuard ],
    component: PointListComponent
  },
  {
    path: 'point/register',
    canActivate: [ AuthGuard ],
    component: PointRegisterComponent
  },
  {
    path: 'point/report',
    canActivate: [ AuthGuard ],
    component: PointReportComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PointRoutingModule { }
