import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { merge, Subscription } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { User, UserToken } from 'src/app/core/models/user';
import { AuthenticationService } from 'src/app/core/services/authentication.service';

import { Point } from '../models/point';
import { PointService } from '../services/point.service';

@Component({
  selector: 'app-point-list',
  templateUrl: './point-list.component.html',
  styleUrls: ['./point-list.component.css'],
})
export class PointListComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['name', 'date', 'recordType'];
  dataSource: MatTableDataSource<Point>;
  panelOpenState = true;
  totalRecords: number;
  pageIndex: number;
  pageSize: number;
  user: User;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private pointService: PointService,
    private authenticationService: AuthenticationService) {
    this.dataSource = new MatTableDataSource();
    
    this.user = this.authenticationService.getUserValue().user;
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.listPoint(
            this.sort.start,
            this.sort.direction,
            this.paginator.pageIndex,
            this.paginator.pageSize
          );
        }),
        map((data: any) => {
          this.totalRecords = data.data.totalRecords;
          this.pageIndex = data.data.pageIndex;
          this.pageSize = data.data.pageSize;
          return data.data;
        })
      )
      .subscribe((data) => {
        this.dataSource.data = data.records;
        this.paginator.length = data.totalRecords;
      });
  }

  ngOnInit(): void {}

  listPoint(sort: any, sortDirection: any, pageIndex: any, pageSize: any) {
    let filter = {
      idCollaborator: this.user.id,
      sort: sort,
      sortDirection: sortDirection,
      pageIndex: pageIndex,
      pageSize: pageSize,
    };

    return this.pointService.listPage(filter);
  }
}
