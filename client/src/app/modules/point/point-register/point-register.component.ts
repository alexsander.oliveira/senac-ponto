import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { User, UserToken } from 'src/app/core/models/user';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { PointRecord } from '../models/point-record';
import { PointService } from '../services/point.service';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-point-register',
  templateUrl: './point-register.component.html',
  styleUrls: ['./point-register.component.css']
})
export class PointRegisterComponent implements OnInit {
  form: FormGroup;
  userRegister: User;
  authenticatedSubscription: Subscription;
  
  constructor(
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private pointService: PointService,
    private matSnacbarMessage: MatSnackBar) { 

      this.userRegister = this.authenticationService.getUserValue().user;

      this.form = this.fb.group({
        name: ['', Validators.required],
        recordType: ['', Validators.required]
      });
  }
  
  ngOnInit(): void {
    
  }

  onSubmit() {
    if ( this.form.invalid ) {
      return;
    }

    var pointRecord = new PointRecord(
      this.userRegister.id,
      this.form.get('name').value,
      this.getDate(new Date()),
      this.form.get('recordType').value
    );

    this.pointService.pointRecord(pointRecord).subscribe((res) => {
      this.matSnacbarMessage.open('Registro de ponto efetuado com sucesso', 'Sucesso', {
        duration: 2000,
       });

       this.form.reset();
    });
  }

  getDate(date: Date) {
    return moment(date).format("YYYY-MM-DDTHH:mm:ss.SSS");
  }
}
