export class PointListReport {
    idCollaborator: string;
    name: string;
    date: Date;
    hoursWorked: number;
}