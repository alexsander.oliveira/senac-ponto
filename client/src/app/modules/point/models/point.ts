export class Point {
    id: string;
    name: string;
    dateRecord: Date;
    typeRecord: string;
}