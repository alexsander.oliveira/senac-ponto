export class PointRecord {
    /**
     *
     */
    constructor(
        idCollaborator: string,
        name: string,
        date: string,
        recordType: string
        ) {
        this.idCollaborator = idCollaborator;
        this.name = name;
        this.date = date;
        this.recordType = recordType;
    }

    idCollaborator: string;
    name: string;
    date: string;
    recordType: string;
}