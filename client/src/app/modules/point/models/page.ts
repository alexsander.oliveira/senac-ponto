export class Page {
    records: any[];
    pageIndex: number;
    pageSize: number;
    totalRecords: number;
}