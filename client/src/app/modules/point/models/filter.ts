export class Filter {
    sort: string;
    sortDirection: string;
    pageIndex: number;
    pageSize: number;
}