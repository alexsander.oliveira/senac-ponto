import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule,
  MatCardModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginator,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule } from '@angular/material';

import { PointListComponent } from './point-list/point-list.component';
import { RecordTypePipe } from './pipes/record.type.pipe';
import { PointRoutingModule } from './point-routing.module';
import { PointRegisterComponent } from './point-register/point-register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PointReportComponent } from './point-report/point-report.component';

@NgModule({
  declarations: [
    PointListComponent,
    PointRegisterComponent,
    RecordTypePipe,
    PointReportComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatExpansionModule,
    MatIconModule,
    MatButtonModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    PointRoutingModule
  ]
})
export class PointModule { }
