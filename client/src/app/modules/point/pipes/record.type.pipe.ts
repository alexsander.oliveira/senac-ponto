import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'recordtypepipe'})
export class RecordTypePipe implements PipeTransform {
  transform(value: string): string {
    if (value === 'E') {
        return 'Entrada';
    } else if ( value ==='S') {
        return 'Saída';
    }

    return '';
  }
}