import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

import { Page } from '../models/page';
import { Collaborator } from '../models/collaborator';

import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient) { }

  listPage(filter: any) : Observable<any> {
    var response = this.http.get<any>(`${environment.serverUrl}Report/point`, 
    { params: new HttpParams({ fromObject: filter }) });
    return response;
  }

  listAllCollaborators() : Observable<Collaborator[]> {
    var response = this.http.get<Collaborator[]>(`${environment.serverUrl}Collaborator/list`);
    return response;
  }
}
