import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

import { PointRecord } from '../models/point-record';
import { Page } from '../models/page';
import { Filter } from '../models/filter';

import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PointService {

  constructor(private http: HttpClient) { }

  pointRecord( pointRecord: PointRecord ) : Observable<PointRecord> {
    return this.http.post<PointRecord>(`${environment.serverUrl}Point/record`,
     pointRecord);
  }

  listPage(filter: any) : Observable<Page> {
    var response = this.http.get<Page>(`${environment.serverUrl}Point/list-page`, 
    { params: new HttpParams({ fromObject: filter }) });
    return response;
  }
}
