import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        private authenticationService: AuthenticationService,
        private router: Router,
        private matSnackBar: MatSnackBar) { }

    intercept(
            request: HttpRequest<any>,
            next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {

                this.authenticationService.logout();
                this.router.navigate(['']);
            }

            if (err.status === 403) {
                this.matSnackBar.open("O usuário não está autorizado a acessar esse recurso.", "Error", {
                    duration: 5000 
                });
            }
            
            const error = err.error.message || err.statusText;
            this.showErrors(err);
            return throwError(error);
        }))
    }

    showErrors(err: any) {
        if (err && err.error.errors ) {
            err.error.errors.forEach(element => {
                this.matSnackBar.open(element, "Error", {
                    duration: 5000 
                })
            });
        }
    }
}