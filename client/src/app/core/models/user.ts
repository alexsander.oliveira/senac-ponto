﻿export class UserToken {
    access_token?: string;
    user: User;
}

export class User {
    id: string;
    userName: string;
}