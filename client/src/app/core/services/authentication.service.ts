import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

import { User, UserToken } from '../models/user';
import { UserLogin } from '../models/user.login';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<UserToken>;
    public currentUser: Observable<UserToken>;
    public isAuthenticated = new BehaviorSubject<boolean>(false);

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<UserToken>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): UserToken {
        return this.currentUserSubject.value;
    }

    checkAuthenticated() {
        var user = localStorage.getItem('currentUser');
        const authenticated = (user !== null && user !== undefined && user !== '') ? true : false;
        this.isAuthenticated.next(authenticated);
        return authenticated;
    }

    getUserValue() : UserToken {
        var userToken = JSON.parse(localStorage.getItem('currentUser'));
        return userToken;
    }

    login(user: UserLogin) {
        return this.http.post<any>(`${environment.serverUrl}User/login`, user)
            .pipe(map(user => {
                localStorage.setItem('currentUser', JSON.stringify(user.data));
                this.currentUserSubject.next(user);
                this.isAuthenticated.next(true);
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this.isAuthenticated.next(false);
    }   
}