import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, OnDestroy {
  title = 'Senac Ponto';
  isAuthenticated: boolean;
  authenticatedSubscription: Subscription;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router) { 
    this.authenticationService.checkAuthenticated();
    
    this.authenticatedSubscription = this.authenticationService.isAuthenticated.subscribe(
      (isAuthenticated: boolean)  => this.isAuthenticated = isAuthenticated
    );
  }

  ngOnInit(): void {
    
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['login']);
  }

  ngOnDestroy(): void {
    if ( this.authenticatedSubscription ) {
      this.authenticatedSubscription.unsubscribe();
    }
  }
}
