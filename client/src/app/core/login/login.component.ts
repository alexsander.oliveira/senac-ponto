import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { UserLogin } from '../models/user.login';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  public loginInvalid: boolean;
  private formSubmitAttempt: boolean;
  private returnUrl: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private matSnacbarMessage: MatSnackBar
    ) { 
    
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.email],
      password: ['', Validators.required]
    });

    if ( this.authenticationService.checkAuthenticated() ){
        this.router.navigate(['']);
    }
  }

  onSubmit() {
    
    if ( this.form.invalid ) {
      return;
    }

    const username = this.form.get('username').value;
    const password = this.form.get('password').value;
    var userLogin = new UserLogin(username, password);
    this.authenticationService.login(userLogin).subscribe((res) => {
        this.matSnacbarMessage.open('Login efetuado com sucesso', 'Sucesso', {
          duration: 2000,
        });
        this.router.navigate(['']);
    });
  }

}
